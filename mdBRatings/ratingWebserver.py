from flask import Flask, render_template, url_for
import checkRatings
import json
#need: sudo iptables -I INPUT -p tcp --dport 5000 -j ACCEPT

#website to view the current ratings from our test buttons
#http://127.0.0.1:5000/


app = Flask('ratingapp')

@app.route('/')
def index():
    ratingLists = checkRatings.getRatingsFromDB()
    return render_template('index.html', ratingStrings=json.dumps(ratingLists[0]),
        foreverValues=ratingLists[1],
        dailyValues=ratingLists[2],
        weeklyValues=ratingLists[3])

if __name__ == '__main__':
    app.run(host= '0.0.0.0')
    app.add_url_rule('/favicon.ico', redirect_to=url_for('static', filename='favicon.ico'))
