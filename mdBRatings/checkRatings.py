#access the remote dynamoDB and check some ratings
#based on how to:
#https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.Python.04.html

#run it directly to see the output
#import to just get access to getRatingsFromDB()
#check __main__ to see output formatat (list of lists)

import boto3
import json
from boto3.dynamodb.conditions import Key, Attr
from collections import Counter
from datetime import *

def getRatingsFromDB():
    dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
    #client = boto3.client('dynamodb',
    table = dynamodb.Table('RatingRecords')

    areaToSearch = 'ARSProgramming'

    #entries have RatingValues Area, Timestamp, RatingValue
    #current timestamp = int(datetime.utcnow().timestamp())

    DAY = timedelta(1)
    WEEK = timedelta(7)
    now = datetime.utcnow()
    dayTimeCutoff = datetime.utcnow() - DAY
    dayTimeCutoff = int((dayTimeCutoff - datetime(1970,1,1)).total_seconds())
    weekTimeCutoff = datetime.utcnow() - WEEK
    weekTimeCutoff = int((weekTimeCutoff - datetime(1970,1,1)).total_seconds())
    #print('one day ago was ', dayTimeCutoff)
    #print('one week ago was ', weekTimeCutoff)

    #get FOREVER info
    responseForever = table.query(
        ProjectionExpression="Area, #ts, RatingValue",
        ExpressionAttributeNames={"#ts": "Timestamp"}, #because Timestamp is reserved
        KeyConditionExpression=Key('Area').eq(areaToSearch)
    )
    foreverRatings = [];
    for i in responseForever[u'Items']:
        foreverRatings.append(i['RatingValue'])

    foreverCount = Counter(foreverRatings)
    #print foreverCount

    #get the ONE DAY info:
    responseDay = table.query(
        ProjectionExpression="Area, #ts, RatingValue",
        ExpressionAttributeNames={"#ts": "Timestamp"}, #because Timestamp is reserved
        KeyConditionExpression=Key('Area').eq(areaToSearch) & Key("Timestamp").gt(dayTimeCutoff)
    )
    dailyRatings = [];
    for i in responseDay[u'Items']:
        dailyRatings.append(i['RatingValue'])
    #print dailyRatings
    dailyCount = Counter(dailyRatings)
    #print dailyCount

    #get the ONE WEEK info:
    responseWeek = table.query(
        ProjectionExpression="Area, #ts, RatingValue",
        ExpressionAttributeNames={"#ts": "Timestamp"}, #because Timestamp is reserved
        KeyConditionExpression=Key('Area').eq(areaToSearch) & Key("Timestamp").gt(weekTimeCutoff)
    )
    weeklyRatings = [];
    for i in responseWeek[u'Items']:
        weeklyRatings.append(i['RatingValue'])

    #print weeklyRatings
    weeklyCount = Counter(weeklyRatings)
    #print weeklyCount
    orderedRatings = ['Good', 'OK', 'Bad']
    foreverValues = [foreverCount[x] for x in orderedRatings]
    dailyValues = [dailyCount[x] for x in orderedRatings]
    weeklyValues = [weeklyCount[x] for x in orderedRatings]
    return [orderedRatings, foreverValues, dailyValues, weeklyValues]

if __name__ == "__main__":
    allResponses = getRatingsFromDB();
    #Now we have all of the info. We just need to stick that in a pretty plot and show it off :-)
    orderedRatings = allResponses[0]
    foreverValues = allResponses[1]
    dailyValues = allResponses[2]
    weeklyValues = allResponses[3]
    print(orderedRatings)
    print(foreverValues)
    print(dailyValues)
    print(weeklyValues)
