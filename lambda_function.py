'''
This is a sample Lambda function that sends an email on click of a
button. It requires these SES permissions.
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ses:GetIdentityVerificationAttributes",
                "ses:SendEmail",
                "ses:VerifyEmailIdentity"
            ],
            "Resource": "*"
        }
    ]
}

The following JSON template shows what is sent as the payload:
{
    "serialNumber": "GXXXXXXXXXXXXXXXXX",
    "batteryVoltage": "xxmV",
    "clickType": "SINGLE" | "DOUBLE" | "LONG"
}

A "LONG" clickType is sent if the first press lasts longer than 1.5 seconds.
"SINGLE" and "DOUBLE" clickType payloads are sent for short clicks.

For more documentation, follow the link below.
http://docs.aws.amazon.com/iot/latest/developerguide/iot-lambda-rule.html
'''

from __future__ import print_function

import boto3
import json
import logging
from botocore.exceptions import ClientError
from datetime import datetime

logger = logging.getLogger()
logger.setLevel(logging.INFO)

ses = boto3.client('ses')
sourceAddress = 'ars-doorbell@umich.edu'  # change it to your email address

def lambda_handler(event, context):
    logging.info('Received event: ' + json.dumps(event))

    if not check_email(sourceAddress):
        logging.error('Email is not verified')
        return
    dbItem = getDBRecord(event['serialNumber'])
    
    if 'RatingArea' in dbItem:
        #we have a rating to log
        #after logging we could have email/slack notifications also
        location = dbItem['RatingArea']
        value = dbItem['RatingValue']
        logRating(location, value)
    
    if 'email' in dbItem:
        #nothing else will happen if email is not verified
        #this prevents other notifications for unverified email addresses
        #is this what we want? I dont know.
        if not check_email(dbItem['email']['address']):
            return
        subject = 'Hello from your IoT button %s' % event['serialNumber']
        body_text = dbItem['email']['message']
        toAddress = dbItem['email']['address']
        ses.send_email(Source=sourceAddress,
                       Destination={'ToAddresses': [toAddress]},
                       Message={'Subject': {'Data': subject}, 'Body': {'Text': {'Data': body_text}}})
        logging.info('Email has been sent')
        
    if 'Stride' in dbItem:
        sendToStride(dbItem['Stride']['url'], json.loads(dbItem['Stride']['headers']), dbItem['Stride']['message'])
        
    if 'phone' in dbItem:
        sendSMS(dbItem['phone']['number'], dbItem['phone']['message'])
        
    
def sendToStride(url, headers, message):
    #headers should include auth and message = text/plain
    #message should be a plain text message to send
    #url is the person/conversation specific url
    from botocore.vendored import requests
    r = requests.post(url, headers=headers, data=message)
    logging.info('stride?'+ r.text)
    

# Check whether email is verified. Only verified emails are allowed to send emails to or from.
def check_email(email):
    result = ses.get_identity_verification_attributes(Identities=[email])
    attr = result['VerificationAttributes']
    if (email not in attr or attr[email]['VerificationStatus'] != 'Success'):
        logging.info('Verification email sent. Please verify it.')
        ses.verify_email_identity(EmailAddress=email)
        return False
    return True

    
def sendSMS(number, message):
    smsClient = boto3.client("sns")
    messageID = smsClient.publish(PhoneNumber=number, Message=message)
    logger.info("message sent with ID", messageID)

def logRating(location, value):
    dynamodb = boto3.resource("dynamodb", region_name='us-east-1')
    table = dynamodb.Table('RatingRecords')
    print('logging with time of', datetime.utcnow().timestamp())
    try:
        response = table.put_item(
            Item = {'Area': location,
            #'Timestamp':datetime.now(tz='UTC').timestamp(),
            'Timestamp': int(datetime.utcnow().timestamp()),
            'Value': value}
            )
            # {'Area':{'S':location},
            #     'Timestamp':{'N':datetime.now(tz=timezone.utc).timestamp()},
            #     'Value': {'S':value}}
                # Key={'Area': location,
                #     'Timestamp', datetime.now(tz=timezone.utc).timestamp()
                #     },
                # Value = {'value': value}
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print('recorded rating to', response)

def getDBRecord(serialNumber):
    dynamodb = boto3.resource("dynamodb", region_name='us-east-1')
    table = dynamodb.Table('iotButtons')
    try:
        response = table.get_item(
                Key={'serialNumber': serialNumber
                    }
        )
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        item = response['Item']
        print('got item:', item)
        return item
