import boto3
import json
from botocore.exceptions import ClientError

dynamodb = boto3.resource("dynamodb", region_name='us-west-2', endpoint_url="http://localhost:8000")
table = dynamodb.Table('iotButtons')

serialToGet = "testSN"

try:
    response = table.get_item(
            Key={'serialNumber': serialToGet
                }
    )
except ClientError as e:
    print(e.response['Error']['Message'])
else:
    item = response['Item']
    print('got item with', item['serialNumber'], 'and phone number', item['phone']['number'])


