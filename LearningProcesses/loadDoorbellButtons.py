from __future__ import print_function # Python 2/3 compatibility
import boto3
import json
import decimal

dynamodb = boto3.resource('dynamodb', region_name='us-west-2', endpoint_url="http://localhost:8000")

table = dynamodb.Table('iotButtons')

with open("doorbellData.json") as json_file:
    buttons = json.load(json_file, parse_float = decimal.Decimal)
    for button in buttons:
        serialNumber = button['serialNumber']
        stride = button['Stride']
        email= button['email']
        phone = button['phone']

        print("Adding button:", serialNumber, stride, email, phone)

        table.put_item(
           Item={
               'serialNumber': serialNumber,
               'Stride': stride,
               'email': email,
               'phone': phone
            }
        )
